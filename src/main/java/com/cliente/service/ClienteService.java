package com.cliente.service;

import com.cliente.model.Cliente;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class ClienteService {
    @Inject
    MongoClient mongo;
    
    @ConfigProperty(name = "mongo.database") //Database name configured in application.properties
    String database;
    
    @ConfigProperty(name = "mongo.collection") //Database collection configured in application.properties
    String collection;
    
    public List<Cliente> listAllClientes(){
        List<Cliente> clientes = new ArrayList();
        MongoCursor<Document> cursor = getCollection().find().iterator();
        
        try{
            while(cursor.hasNext()){
                Document document = cursor.next();
                Cliente cliente = new Cliente();
                cliente.setFechaNacimiento(document.getDate("fechaNacimiento").toInstant().atZone(ZoneId.of("UTC")).toLocalDate());
                cliente.setNombres(document.getString("nombres"));
                cliente.set_id(document.getObjectId("_id"));
                cliente.setDocumentoIdentificacion(document.getString("documentoIdentificacion"));
                cliente.setApPaterno(document.getString("apPaterno"));
                cliente.setApMaterno(document.getString("apMaterno"));
                clientes.add(cliente);
            }
        }catch (Exception ex) {
            System.out.println("No se puede obtener la lista de clientes");
            ex.printStackTrace();
        }finally{
            cursor.close();
        }
        return clientes;
    }
    
    public Cliente getClienteById(String id){
        Document documentCliente = (Document)getCollection().find(Filters.eq("_id", new ObjectId(id))).first();
        Cliente cliente = new Cliente();
        cliente.set_id(documentCliente.getObjectId("_id"));
        cliente.setDocumentoIdentificacion(documentCliente.getString("documentoIdentificacion"));
        cliente.setFechaNacimiento(documentCliente.getDate("fechaNacimiento").toInstant().atZone(ZoneId.of("UTC")).toLocalDate());
        cliente.setNombres(documentCliente.getString("nombres"));
        cliente.setApPaterno(documentCliente.getString("apPaterno"));
        cliente.setApMaterno(documentCliente.getString("apMaterno"));
        return cliente;
    }
    
    public Cliente addCliente(Cliente cliente){
        Document documentCliente = new Document()
            .append("documentoIdentificacion", cliente.getDocumentoIdentificacion())
            .append("fechaNacimiento", cliente.getFechaNacimiento())
            .append("nombres", cliente.getNombres())
            .append("apPaterno", cliente.getApPaterno())
            .append("apMaterno", cliente.getApMaterno());
        
        ObjectId insertedId = getCollection().insertOne(documentCliente).getInsertedId().asObjectId().getValue();
        cliente.set_id(insertedId);
        return cliente;
    }
    
    
    
    private MongoCollection getCollection(){
        return mongo.getDatabase(database).getCollection(collection);
    }
}
