package com.cliente.resource;

import com.cliente.model.Cliente;
import com.cliente.service.ClienteService;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class ClienteResource {
    
    @Inject
    @Singleton
    ClienteService service;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Cliente> getClientes(){
        return service.listAllClientes();
    }
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente getClienteById(@PathParam("id") String id){
        return service.getClienteById(id);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente postCliente(Cliente cliente){
        return service.addCliente(cliente);
    }
}
